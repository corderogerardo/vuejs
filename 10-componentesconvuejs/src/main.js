var Vue = require('vue')
/*Instancia de Vue*/
new Vue({
	
	el: 'body',

	
	directives: {
		flip: require('./directives/flip')
	},
	filters:
	{

		reverse: require('./filters/reverse')

	},
	components: {
		a: require('a'),
		b: require('b')

	},
	data:
	{

		msg:'hello',
		value:'esto deberia estar al reves y flipeado',
		bgColor:'#f3f3f3',

	},
	

})