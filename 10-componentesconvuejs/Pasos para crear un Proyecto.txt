Pasos Para comenzar a crear un proyecto.
Paso Cero:
	Saber git, tener una cuenta en github o gitlab, conectar nuestro proyecto, mantener nuestro trabajo siempre con nuestro sistema de control de versiones.
Primer Paso:
	Realizar un diseño grafico del sitio web con photoshop o illustrator.
	Reconocer que necesitas un componente web, es decir debes saber que es un componente web y para que se usa.
Segundo Paso:
	Elegir la herramienta con la que vas a trabajar tus componentes:
		*Polymer
		*Angularjs
		*Reactjs
		*VueJS ** este caso
Tercer Paso:
	Debes trabajar tus proyectos siempre pensando en 2 Etapas:
		Etapa 1: Desarrollo.
		Etapa 2: Produccion.
Nota: Es bueno aprender a trabajar con GULP-Browserify - Grunt - NPM - de tal forma que podamos tener nuestros archivos separados - organizados en nuestra etapa de "Desarrollo" y unirlos compilarlos, optimizarlos para subirlos a nuestra nube "PRODUCCION".

Cuarto Paso:
	Organizar npm e instalar dependencias, configurar GULP y todas las dependencias que usaremos para desarrollar un proyecto.

Quinto Paso:
	Crear nuestros diseños front-end, diseñar cada componente, vista que utilizaremos, una vez que tenemos el diseño, podemos comenzar a programar nuestro sitio web.

Sexto Paso:
	Crear carpetas donde tendremos nuestros archivos en nuestra Etapa de Desarrollo.

Septimo Paso:
	Correr NPM, GULP para poder ir probando nuestros archivos, en una etapa que podemos llamar preproduccion.



SIEMPRE QUE CREAMOS COMPONENTES CON VUEJS DEBEMOS HACER
	1. Crear carpeta de componentes.
	2. Crear HTML y CSS
	3. Crear archivo JS para exportar nuestro componente.
	4. Crear nuestro archivo component.json y configurar.
	5. Crear carpetas de directivas y filtros, crear nuestras directivas de vuejs y filtros personalizados.
	6. Crear nuestro archivo js principal con la nueva instancia de vuejs.
		6.1 Requerir el vue.js.
		6.2. Definir el elemento que contendra los componentes.
		6.3 Requerir nuestras directivas y filtros, componentes.
		6.4 Crear los atributos o variables de la data de nuestro componente.
		6.5 Compilar los componentes y estilos en un solo archivo para produccion.