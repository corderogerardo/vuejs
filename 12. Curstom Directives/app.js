Vue.directive('write', function(message){

	this.el.textContent = message;

});

/*Vue.directive('confirm', function(message){
	this.el.addEventListener("click", function(e){
		if(!confirm(message)){
			e.preventDefault();
		return;
		}
	});
});*/

//OTRA FORMA DE TRABAJAR CON DIRECTIVAS PERSONALIZADAS.

Vue.directive('confirm',{

	isLiteral: true,

	bind: function(){

		this.el.addEventListener('click',function(e){
			if(!confirm(this.expression)){
				e.preventDefault();
				return;
			}
		}.bind(this));
	}

});

Vue.directive('example',{

	acceptStatement: true,

	update: function(expr){

		expr();
	
	}
});

/*Instancia de Vue*/
new Vue({
	
	el: 'body',

	data:
	{
		message: 'Hello World',

		age:1,
		

	},
	components:{

		

	},
	directives:{

		
	},
	computed:
	{

	},
	filters:
	{

		

	},
	methods:
	{

		check: function(){
			alert('Form was submitted');
		}

	},

});