/*Instancia de Vue*/
new Vue({
	
	el: '#post',

	data:
	{
		esVisible: true,
		liked: false,
		likesCount: 10

	},
	computed:
	{

	},
	filters:
	{

	},
	methods:
	{

		toggleLike: function()
		{
			this.liked = ! this.liked;

			this.liked ? this.likesCount++ : this.likesCount--;
		},

	},

})