/*Instancia de Vue*/
new Vue({
	
	el: '#demo',

	data:
	{

		names:['Gerardo', 'Maria', 'Gaby', 'Jose','Jesus'],
		txtName:'',

	},
	computed:
	{

	},
	filters:
	{

	},
	methods:
	{

		agregarNombre: function()
		{

			this.names.push(this.txtName);
			this.txtName='';

		}

	},

})
