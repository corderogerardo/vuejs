new Vue({
	// asi es como inicializas el componente o etiqueta que vas a trabajar con vuejs
	el: '#tasks',
	//aqui es donde iria el modelo, o tu array de objetos que vas a mostrar en la vista, o a validar, podriamos decir que es donde trabajaras todos tus atributos de tu clase, junto con los inputs del formulario con el que trabajan.
	data: 
	{
		//objeto json, arreglo de objetos json
		tasks:
		[
		{
			body:'Go to the store',completed:false
		}
		],
		//declaras el input o atributo, para poder utilizarlo en los metodos.
		newTask: ''
	},
	/* 
		Computed me permite crear funciones personalizadas
		que podemos utilizar en divs, o cualquier componente que creemos, se usa junto con directivas v-if, v-show, o directivas personalizadas.
	*/
	computed: {
		completions: function()
		{
			return this.tasks.filter(function(task){
				return task.completed;
			});
		},

		remaining: function()
		{

			return this.tasks.filter(function(task)
			{
				return ! task.completed;
			});

		},
	},

	/*Aqui vamos a colocar los filtros que deseamos agregarle a nuestro elemento-componente-vista, como deseen llamarle*/

	filters: {
		inProcess: function(tasks)
		{
			return tasks.filter(function(task){
				return ! task.completed;
			});
		}
	},

	//aqui es donde creas tus metodos y les dices que van a hacer exactamente en la vista, en la vista los utilizas.
	methods:
	{
		addTask: function (e)
		{
			e.preventDefault();

			//valida que no se agreguen campos vacios.
			if (! this.newTask) return;

			this.tasks.push({
				body:this.newTask,
				completed:false
			});
			this.newTask='';
		},

		editTask: function(task)
		{
			/*remove the task*/
			this.removeTask(task);
			/* update the newTask input*/
			this.newTask = task.body;
			/*focus the newTask input*/
			this.$$.newTask.focus();
		},

		toggleTaskCompletion: function(task)
		{
			task.completed = ! task.completed;
		},

		removeTask: function(task){
			this.tasks.$remove(task);
		},
		completeAll: function(){

			this.tasks.forEach(function(task)
			{
				task.completed = true;
			});

		},

		clearCompleted: function()
		{
			this.tasks = this.tasks.filter(function(task){
				return ! task.completed;
			});
		}

	}

})