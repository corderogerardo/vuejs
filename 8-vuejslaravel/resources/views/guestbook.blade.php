<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta  id="token" name="token" value="{{  csrf_token() }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Guestbook</title>
	
	<link rel="stylesheet" href="">

</head>
<body>

	<div id="guestbook">

	<form method="POST" v-on="submit: onSubmitForm">

		<div id="form-group">	
			<label for="name">Nombre: 
			<span class="errors" v-if="! newMessage.name">*</span>
			</label>
			<input name="name" id="name" type="text" v-model="newMessag	e.name">
			</input>
		</div>	
		<div id="form-group">	
			<label for="message">Mensaje: 
			<span class="errors" v-if="! newMessage.message">*</span>
			</label>
			<textarea name="message" id="message" type="text" v-model="newMessage.message">
			</textarea>
		</div>	
		<div id="form-group" v-if="! submitted">
		<button type="submit" v-attr="disabled: errors">Inscribirse en lista de invitados</button>
		</div>
		
		<div class="alert alert-success" v-if="submitted">Thanks!</div>
	</form>
		<hr>
		<article v-repeat="messages">

			<h3> @{{ name }} </h3>

			<div class="messageBody">
				@{{ message }}
			</div>
			
		</article>

		<!-- <pre>
			@{{$data | json}}
		</pre> -->

	</div>


	<script src="js/vendor.js"></script>
	<script src="js/guestbook.js"></script>
</body>
</html>