Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

/*Instancia de Vue*/
new Vue({
	
	el: '#guestbook',

	ready: function(){

		this.fetchMessages();

	},

	data:
	{

		newMessage: {
			name:'',
			message:'',
		},

		submitted: false,

	},
	computed:
	{

		errors: function(){

			for(var key in this.newMessage){

				if(! this.newMessage[key]) return true;
			}
			return false;

		},

	},
	filters:
	{

	},
	methods:
	{
		fetchMessages: function()
		{
			/*vuejs recibe los datos desde la ruta que creamos en laravel*/
			this.$http.get('api/messages', function(messages){

				this.$set('messages',messages);

			});
		},

		onSubmitForm: function(e){
			// prevent de default accion.

			e.preventDefault();

			/*creamos una variable mensaje le asignamos el nuevo mensaje, para poder enviarlo a la base de datos y tenerlo de manera persistente*/

			var message = this.newMessage;

			// add new message to 'messages' array

			this.messages.push(message);

			/*reset input values*/

			this.newMessage= {name:'',message:''};

			/*send POST ajax request*/

			this.$http.post('api/messages',message);

			/*show thanks message*/

			this.submitted = true;

			/*hide submit button*/
			/* v-if="! submitted" */

		},
	},

});