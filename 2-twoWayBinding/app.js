/*Instancia de Vue*/
new Vue({
	
	el: "#demo",

	data: {
		name: 'foobar',
		number:4,
	},

	ready: function() {

		var that = this;

		setInterval(function(){

			that.name ='Updated';
			that.number = 5;

		}, 5000);

	},

})