/*Instancia de Vue*/
new Vue({
	
	el: '#demo',

	data:
	{

	},
	computed:
	{

	},
	filters:
	{

	},
	methods:
	{
		onKeyUp: function()
		{
			alert("Utilizando eventos y listeners con VueJS.");
		},
		onBlur: function()
		{
			alert("Utilizando on blur");
		},
	},

})