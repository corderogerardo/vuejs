// filtros globales fuera de las instancias de vue
// para no repetir codigo, DRY

Vue.filter('reverse', function(value, wordsOnly){
	var separador = '';

	wordsOnly && (separador = ' ');

	return value.split(separador).reverse().join(separador);

});

/*Instancia de Vue*/
new Vue({
	
	el: '#demo',

	data:
	{

		message: 'Hello world',

	},
	computed:
	{

	},
	/*Aqui trabajare mis filtros personalizados*/
	filters:
	{

		/*reverse: function(value, wordsOnly)
		{*/
			

			/*javascript tiene metodos para trabajar los strings
			split('') separa los valores posicion a posicion
			reverse() invierte la posicion
			join('') une de nuevo las posiciones invertidas
			
			*/
				//return value.split('').reverse().join('');

			/*Si solamente quieres invertir las palabras*/
			
		
		
		//}

	},
	methods:
	{

	},

});

/*Instancia de Vue*/
new Vue({
	
	el: '#demo2',

	data:
	{

		message:'Hello again',

	},
	computed:
	{

	},
	filters:
	{

	},
	methods:
	{

	},

});

/*Instancia de Vue*/
new Vue({
	
	el: '#demo3',

	data:
	{
		gender:'all',

		people:[
		
		{ name:'Gerardo', gender:'male'},
		{ name:'Jack', gender:'male'},
		{ name:'Kate', gender:'female'},
		{ name:'Claire', gender:'female'},


		]

	},
	computed:
	{

	},
	filters:
	{

		gender: function(people){
			var self = this;

			if(this.gender == 'all'){
				return people;
			}

			return people.filter(function(person){
				return person.gender == self.gender;
			});
		},

		/*Otra forma, para no hacer multibinding*/

		genderOpcional: function(people){
			if (this.gender=='all') return people;

			return people.filter(function(person){
				return person.gender == this.gender;
			}.bind(this));
		},

	},
	methods:
	{

	},

})