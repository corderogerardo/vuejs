/*Instancia de Vue*/
new Vue({
	
	el: '#demo',

	data:
	{
		sortKey: '',

		reverse:false,

		search:'',

		columns : ['name','age'],

		people:[

			{name: 'John', age:50},
			{name: 'Juan', age:20},
			{name: 'Gerardo', age:26},
			{name: 'Maria', age:22},
			{name: 'Nancy', age:23},
			{name: 'Antonieta', age:24},
			{name: 'Gaby', age:26},


		]

	},
	computed:
	{

	},
	filters:
	{

	},
	methods:
	{

		ordenarPor: function(sortKey)
		{
			this.reverse = (this.sortKey == sortKey) ? ! this.reverse: false;

			this.sortKey = sortKey;
		}

	},

})