Vue.component('coupon', {
	
	template:'#coupon-template',
	
	props: ['when-applied'],

	data: function() {
		return{
			coupon:'',
			message:''
		}
	},
	methods:{
		whenCouponHasBeenEntered: function(){
			this.validate();
		}
	},
	validate: function(){
		if(this.coupon == 'FOOBAR'){
			this.whenApplied(this.coupon);
			return this.message = '20% Off!';
		}
		this.message = 'That coupon does not exist';
	}
});
/*Instancia de Vue*/
new Vue({
	
	el: '#demo',

	components:{
	},
	directives:{
	},
	computed:
	{
	},
	filters:
	{
	},
	methods:
	{
		setCoupon: function(coupon){
		this.$set('coupon',coupon);
		//alert('set coupon: '+ coupon);

		}

	},

});